import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {FormsModule } from '@angular/forms'

import { IngredientComponent } from './ingredient.component';
import Ingredient from './ingredient.class';

describe('IngredientComponent', () => {
  let component: IngredientComponent;
  let fixture: ComponentFixture<IngredientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngredientComponent ],
      imports: [FormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngredientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    
    expect(component).toBeTruthy();
  });

  it('should contains carrot, meat, potato, rice', () => {
    
    expect(component.ingredients).toContain({name: 'carrot', quantity: 1});
    expect(component.ingredients).toContain({name: 'meat', quantity: 1});
    expect(component.ingredients).toContain({name: 'potato', quantity: 1});
    expect(component.ingredients).toContain({name: 'rice', quantity: 1});
  });

  it('should increase and decrease the amount of the ingredient in index 4', () => {
    component.addIng(4, 1);
    expect(component.ingredients[4].quantity).toBe(2);
    component.removeIng(4);
    expect(component.ingredients[4].quantity).toBe(1);
  });

  it('should decrease the amount of the ingredient in index 1 and remove it as the amount will become 0', () => {
    const ingredient = new Ingredient(component.ingredients[1].name, component.ingredients[1].quantity);
    component.removeIng(1);
    expect(component.ingredients).not.toContain(ingredient);
  });

  it('should remove of the ingredient in index 3', () => {
    const ingredient = new Ingredient(component.ingredients[3].name, component.ingredients[3].quantity);
    component.removeAll(3);
    expect(component.ingredients).not.toContain(ingredient);
  });

  it('should add a new Ingredient', () => {
    const quantity = component.ingredients.length;
    component.addNewIng();
    expect(component.ingredients.length).toBeGreaterThan(quantity);
  });
  
  it('should increase the amout of an existing ingredient', () => {
    component.ingredients.push({name: 'testIng', quantity: 1})
    component.ingName = 'testIng';
    component.ingQty = 2;
    component.addNewIng();
    expect(component.ingredients).toContain({name: 'testIng', quantity: 3});
  });

  it('should edit the ingredient in index 2', () => {
    component.editIng(2);
    component.ingName = 'testEdit';
    component.addNewIng();
    expect(component.ingredients).toContain({name: 'testEdit', quantity: 1});
  });

  it('should raise sendToRecipe event when send to recipe is clicked', () => {
    let ingredient: Ingredient = null;
    component.sendToRecipe.subscribe( sr => ingredient = sr);
    component.addToRecipe(1);
    expect(ingredient).toBe(component.ingredients[1]);
  });
});
