import { Component, OnInit } from '@angular/core';
import { Ingredient } from '../shared/item.model';
import { IngredientManagementService } from '../fridgeManager.service'
import { HttpRequest, HttpClient, HttpResponse } from '@angular/common/http';


@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.css']
})
export class IngredientComponent implements OnInit {
  ingredients: Ingredient[] = [];
  ingName: string = '';
  ingQty: number;
  button: {value: string, class: string, index: number} = {value: " Add", class: "fa fa-floppy-o", index: 0};
  

  constructor(private ingredientManager: IngredientManagementService, private http: HttpClient) {
    
  }

  addIngredient(ingredient: Ingredient) {
    this.ingredientManager.addIngredient(ingredient).then((ingredients: Ingredient[])=>{
      this.ingredients = ingredients;
    });
  }
  
  removeIngredient(ingredient: Ingredient){
    this.ingredientManager.removeIngredient(ingredient).then((ingredients: Ingredient[])=>{
      this.ingredients = ingredients;
    });
  }

  editIngredient(index: Number, ingredient: Ingredient){
    this.ingredientManager.editIngredient(index, ingredient).then((ingredients: Ingredient[])=>{
      this.ingredients = ingredients;
    });
  }

  addIng(index: number, quantity: number){
    let ingredient: Ingredient = new Ingredient(this.ingredients[index].name, 1);
    this.addIngredient(ingredient);
  }
  
  removeIng(index: number){
    let ingredient: Ingredient = new Ingredient(this.ingredients[index].name, 1);
    this.removeIngredient(ingredient);
  }

  removeAll(index: number){
    this.removeIngredient(this.ingredients[index]);
  }

  addNewIng(){
    let ingredient = new Ingredient(this.ingName, Number(this.ingQty));
    if (this.button.value == ' Add'){
      this.addIngredient(ingredient);
    }
    else{
      this.button.value = ' Add';
      this.button.class = 'fa fa-floppy-o';
      this.editIngredient(this.button.index, ingredient);
    }
    this.ingName = '';
    this.ingQty = 0;
  }

  editIng(index: number){
    this.ingName = this.ingredients[index].name;
    this.ingQty = this.ingredients[index].quantity;
    this.button.value = " Edit"
    this.button.class = "fa fa-pencil";
    this.button.index = index;
  }

  addToRecipe(index: number){
    this.ingredientManager.sendToRecipe(this.ingredients[index]);

  }

  ngOnInit() {
    this.ingredientManager.getIngredientsList().then((ingredients: Ingredient[])=>{
      this.ingredients = ingredients;
    });
  }
}
