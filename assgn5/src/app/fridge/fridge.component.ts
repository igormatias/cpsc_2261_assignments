import { Component, OnInit, Output} from '@angular/core';
import { Recipe, Ingredient } from '../shared/item.model'
import { RecipeManagementService, FridgeManagementService } from '../fridgeManager.service'

@Component({
  selector: 'app-fridge',
  templateUrl: './fridge.component.html',
  styleUrls: ['./fridge.component.css'],

})
export class FridgeComponent implements OnInit {
  ingredients: Ingredient[] = []
  recipeIngredients: Ingredient[] = [];
  list: {shoppingList: Ingredient[], fridgeIngredients: Ingredient[]};


  constructor(private recipeManagement: RecipeManagementService, private fridgeManagement: FridgeManagementService) {
    this.list = this.fridgeManagement.checkRecipe(this.recipeIngredients);
    this.ingredients = this.fridgeManagement.ingredients;
  }

  ngOnInit() {
    this.recipeManagement.getFridge(this);
  }


  private checkIngInShoppingList(ingredient: Ingredient): number{
    for (let i: number = 0; i < this.list.shoppingList.length; i++){
      if (ingredient.name == this.list.shoppingList[i].name)
        return this.list.shoppingList[i].quantity;
    }
    return 0;
  }

  updateLists(){
    this.list = this.fridgeManagement.checkRecipe(this.recipeIngredients);
    console.log(this.list);
  }

  addIng(index: number){
    let ingredient = new Ingredient(this.ingredients[index].name, 1)
    this.fridgeManagement.addIngredient(ingredient ,this.ingredients);
    this.updateLists();
  }
  
  removeIng(index: number){
    let ingredient = new Ingredient(this.ingredients[index].name, 1)
    this.fridgeManagement.removeIngredient(ingredient ,this.ingredients);
    this.updateLists();
    
  }

  removeAll(index: number){
    let ingredient = new Ingredient(this.ingredients[index].name, this.ingredients[index].quantity)
    this.fridgeManagement.removeIngredient(ingredient ,this.ingredients);    
    this.updateLists();
  }

  addFromShoppingList(index: number){
    let ingredient: Ingredient = new Ingredient(
      this.list.shoppingList[index].name, 
      this.list.shoppingList[index].quantity);    
    this.fridgeManagement.addIngredient(ingredient ,this.ingredients);
    this.updateLists();
  }

  checkIfMissing(ingredient: Ingredient):string{
    let quantity: number = this.checkIngInShoppingList(ingredient);
    if (quantity <= 0)
      return 'ok';
    return 'missing ' + quantity;
  }

  removeOfRecipe(ingredient: Ingredient){
    this.fridgeManagement.removeIngredient(ingredient, this.recipeIngredients);
    this.updateLists();
  }

}
