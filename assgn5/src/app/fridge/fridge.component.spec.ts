import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FridgeComponent } from './fridge.component';
import Ingredient from '../ingredient/ingredient.class';

describe('FridgeComponent', () => {
  let component: FridgeComponent;
  let fixture: ComponentFixture<FridgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FridgeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FridgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add a new ingredient to the fridge', () => {
    component.fridge.contents.push(new Ingredient('test',1));
    component.addIng(new Ingredient('test 2',1));
    expect(component.fridge.contents[0].quantity).toBe(1);
    expect(component.fridge.contents.length).toBe(2);
    expect(component.fridge.contents).toContain(new Ingredient('test 2', 1));
    
  });

  it('should increase the amount of an existing ingredient', () => {
    component.fridge.contents.push(new Ingredient('test',1));
    component.addIng(new Ingredient('test',1));
    expect(component.fridge.contents[0].quantity).toBe(2);
  });

  it('should decrease the amount of ingredient', () => {
    component.fridge.contents.push(new Ingredient('test',2));
    component.removeIng(new Ingredient('test',1));
    expect(component.fridge.contents).toContain(new Ingredient('test', 1));
  });

  it('should decrease the amount of ingredient and remove it as the quantity become 0', () => {
    component.fridge.contents.push(new Ingredient('test',1));
    component.removeIng(new Ingredient('test',1));
    expect(component.fridge.contents).not.toContain(new Ingredient('test', 1));
    expect(component.fridge.contents.length).toBe(0);
  });

  it('should remove an ingredient', () => {
    component.fridge.contents.push(new Ingredient('test',5));
    component.removeAll(new Ingredient('test',5));
    expect(component.fridge.contents).not.toContain(new Ingredient('test', 1));
    expect(component.fridge.contents.length).toBe(0);
  });

  it('update the lists with ingredient surprass in the fridge', () => {
    component.fridge.contents.push(new Ingredient('test',5));
    component.fridge.contents.push(new Ingredient('test 2',1));
    
    component.recipe.ingredients.push(new Ingredient('test',1));
    component.recipe.ingredients.push(new Ingredient('test 2',1));
    
    component.updateLists();
    expect(component.list.fridgeIngredients.length).toBe(2);
    expect(component.list.shoppingList.length).toBe(0);
  });

  it('update the lists with ingredients in the fridge and recipe been the same ones', () => {
    component.fridge.contents.push(new Ingredient('test',5));
    component.fridge.contents.push(new Ingredient('test 2',1));
    
    component.recipe.ingredients.push(new Ingredient('test',5));
    component.recipe.ingredients.push(new Ingredient('test 2',1));
    
    component.updateLists();
    expect(component.list.fridgeIngredients.length).toBe(2);
    expect(component.list.shoppingList.length).toBe(0);
  });

  it('update lists with missing ingredient quantity in the fridge', () => {
    component.fridge.contents.push(new Ingredient('test',1));
    component.fridge.contents.push(new Ingredient('test 2',1));
    
    component.recipe.ingredients.push(new Ingredient('test',5));
    component.recipe.ingredients.push(new Ingredient('test 2',1));
    
    component.updateLists();
    expect(component.list.fridgeIngredients.length).toBe(2);
    expect(component.list.shoppingList.length).toBe(1);
    expect(component.list.shoppingList).toContain(new Ingredient('test', 4));
  });

  it('update lists with missing ingredient and ingredient quantity missing in the fridge', () => {
    component.fridge.contents.push(new Ingredient('test',1));
    component.fridge.contents.push(new Ingredient('test 2',1));
    
    component.recipe.ingredients.push(new Ingredient('test',5));
    component.recipe.ingredients.push(new Ingredient('test 2',1));
    component.recipe.ingredients.push(new Ingredient('test 3',1));
    
    component.updateLists();
    expect(component.list.fridgeIngredients.length).toBe(2);
    expect(component.list.shoppingList.length).toBe(2);
    expect(component.list.shoppingList).toContain(new Ingredient('test', 4));
    expect(component.list.shoppingList).toContain(new Ingredient('test 3', 1));
  });
  

});
