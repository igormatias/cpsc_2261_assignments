import Ingredient from '../ingredient/ingredient.class';

export default class Recipe{
    constructor(public ingredients: Ingredient[], public instructions: string[], public estimatedTime: string) { }

    private addIfExist(ingredient: Ingredient): boolean{
        let found: boolean = false;
        for (let i: number = 0; i < this.ingredients.length && !found; i++){
            if (this.ingredients[i].name == ingredient.name){
                this.ingredients[i].quantity += ingredient.quantity;
                found = true;
            }
        }
        return found;
    }
    
    addItem(ingredient: Ingredient): void{
        if (!this.addIfExist(ingredient))
            this.ingredients.push(ingredient);
    }
    //instructions are not going to be check, just add then already
    addInstruction(instruction: string): void{
        this.instructions.push(instruction);
    }

}