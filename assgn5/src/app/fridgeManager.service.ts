import {Ingredient, Recipe } from './shared/item.model'
import { RecipeComponent } from './recipe/recipe.component'
import { FridgeComponent } from './fridge/fridge.component'
import { HttpRequest, HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core'
import { reject } from 'q';
import { resolve } from 'url';
import { promise } from 'protractor';



class Result{
    found: boolean;
    index: number;
}

class Util{
    checkIfExist(ingredient: Ingredient, ingredients: Ingredient[]): Result{
        let found: boolean = false;
        let index: number = -1;
        for (let i: number = 0; i < ingredients.length && !found; i++){
            if (ingredients[i].name == ingredient.name){
                index = i;
                found = true;
            }
        }
        return {found, index};
    }

    addIngredientToList(ingredient: Ingredient, list: Ingredient[], result: Result){
        if (result.found)
            list[result.index].quantity += ingredient.quantity;
        else
            list.push(ingredient);
    }

    removeIngredient(ingredient: Ingredient, list: Ingredient[], result: Result){
        if (result.found)
            list[result.index].quantity -= ingredient.quantity;
        if (list[result.index].quantity <= 0)
            list.splice(result.index, 1);
    }
    copyIngredients(ingredients: Ingredient[]): Ingredient[]{
        let newIngredients: Ingredient[] = [];
        ingredients.map( (ingredient: Ingredient) =>{
            newIngredients.push(new Ingredient(ingredient.name, ingredient.quantity));
        })
        return newIngredients;
    }

    copyInstructions(instructions: string[]): string[]{
        let newInstructions: string[] = [];
        instructions.map( (instruction: string) =>{
            newInstructions.push(instruction);
        })
        return newInstructions;
    }
}
export class FridgeManagementService{
    private util = new Util();
    ingredients: Ingredient[] = [];
    

    addIngredient(ingredient: Ingredient, ingredients: Ingredient[]){
        let result: Result = this.util.checkIfExist(ingredient, ingredients)
        this.util.addIngredientToList(ingredient, ingredients, result);
    }

    removeIngredient(ingredient: Ingredient, ingredients: Ingredient[]) {
        let result: Result = this.util.checkIfExist(ingredient, ingredients)
        this.util.removeIngredient(ingredient, ingredients, result);
    }
    

    checkRecipe(recipeIngredients: Ingredient[]): {shoppingList: Ingredient[], fridgeIngredients: Ingredient[]}{
        //return two arrays
        //the array in index 0 is the shopping list
        //the array in index 1 is the ingredients that are already in the fridge
        let shoppingList: Ingredient[] = [];
        let fridgeIngredients: Ingredient[] = this.util.copyIngredients(this.ingredients);
        recipeIngredients.map( (ingFromRecipe: Ingredient) =>{
            let result: {found: boolean, index: number} = this.util.checkIfExist(ingFromRecipe, this.ingredients);
            if (!result.found)
                shoppingList.push(ingFromRecipe);
            else{
                let quantity = ingFromRecipe.quantity - this.ingredients[result.index].quantity;
                if (quantity > 0)
                    shoppingList.push(new Ingredient(ingFromRecipe.name, quantity));
            }
        });
        return {shoppingList, fridgeIngredients};
    }
    

}
export class RecipeManagementService{
    recipesList: Recipe[] = [];
    private fridgeComponent: FridgeComponent;
    private util = new Util();
    
    getFridge(fridge: FridgeComponent){
        this.fridgeComponent = fridge;
    }

    addIngredient(ingredient: Ingredient, recipe: Recipe){
        let result: Result = this.util.checkIfExist(ingredient, recipe.ingredients)
        this.util.addIngredientToList(ingredient, recipe.ingredients, result);
    }

    removeIngredient(ingredient: Ingredient, recipe: Recipe) {
        let result: Result = this.util.checkIfExist(ingredient, recipe.ingredients)
        this.util.removeIngredient(ingredient, recipe.ingredients, result);
    }

    addInstruction(instruction: string, recipe: Recipe){
        recipe.instructions.push(instruction);
    }

    removeInstruction(index: number, recipe: Recipe) {
        recipe.instructions.splice(index, 1);
    }

    editInstruction(index: number, instruction: string, recipe: Recipe){
        recipe.instructions[index] = instruction;
    }

    getRecipeList(): Recipe[]{
        return this.recipesList;
    }

    getNewRecipe(){
        return new Recipe('',[],[],'');
    }

    addnewRecipe(recipe: Recipe){
        this.recipesList.push(recipe);
    }

    getRecipe(index: number): Recipe{
        //need to return a new object with new arrays
        let recipe: Recipe = new Recipe(
            this.recipesList[index].name, 
            this.util.copyIngredients(this.recipesList[index].ingredients),
            this.util.copyInstructions(this.recipesList[index].instructions),
            this.recipesList[index].estimatedTime);
        return recipe;
    }

    update(index: number, recipe: Recipe){
        this.recipesList[index] = recipe;
    }

    remove(index: number){
        this.recipesList.splice(index, 1);
    }

    addIngredientToFridge(ingredient: Ingredient, ingredients: Ingredient[]){
        let result: Result = this.util.checkIfExist(ingredient, ingredients)
        this.util.addIngredientToList(ingredient, ingredients, result);
    }
    

    sendToFridge(recipe: Recipe){
        let newIngredients: Ingredient[] = this.util.copyIngredients(recipe.ingredients);
        newIngredients.map( (newIngredient) =>{
            this.addIngredientToFridge(newIngredient, this.fridgeComponent.recipeIngredients)            
        })
        //this.fridgeComponent.recipeIngredients = this.util.copyIngredients(recipe.ingredients);
        this.fridgeComponent.updateLists();
    }
}    

@Injectable()
export class IngredientManagementService{
    private ingredients: Ingredient[] = [];
    private util = new Util();
    private recipeComponent: RecipeComponent;
    private recipeManagement = new RecipeManagementService();

    constructor(private http: HttpClient){

    }

    addIngredient(ingredient: Ingredient): Promise<Ingredient[]> {            
        return new Promise((res)=>{
            this.http.post("http://localhost:3000/ingredients/addIngredient", ingredient)
            .toPromise().then((result:any)=>{
                this.ingredients = result;
                res(this.ingredients);
            });
        });
    }
      
    removeIngredient(ingredient: Ingredient): Promise<Ingredient[]>{
    return new Promise((res) =>{
        this.http.post("http://localhost:3000/ingredients/removeIngredient", ingredient)
        .toPromise().then((result:any)=>{
            this.ingredients = result;
            res(this.ingredients);
            });    
        });
    }
    
    editIngredient(index: Number, ingredient: Ingredient): Promise<Ingredient[]>{
        return new Promise((res) =>{
            let req = {index: index, ingredient: ingredient};
            this.http.post("http://localhost:3000/ingredients/editIngredient", req)
            .toPromise().then((result:any)=>{
                this.ingredients = result;
                res(this.ingredients);
            });    
        });
    }

    getIngredientsList(): Promise<Ingredient[]>{
        return new Promise((res) =>{
            let request = this.http.request(new HttpRequest("GET", "http://localhost:3000/ingredients/getIngredients"))
            .toPromise().then((result:any)=>{
                result.body.map((ingredient: Ingredient)=>{
                    this.ingredients.push(ingredient);
                });
                res(this.ingredients);
            });
        });
    }
    
    getRecipe(recipe: RecipeComponent){
        this.recipeComponent = recipe;
    }

    sendToRecipe(ingredient: Ingredient){
        let newIngredient: Ingredient = new Ingredient(ingredient.name, ingredient.quantity);
        this.recipeManagement.addIngredient(newIngredient, this.recipeComponent.recipe);

    }
}