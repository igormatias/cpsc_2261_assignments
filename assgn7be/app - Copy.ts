import app = require('./config/config');
import { BBTConnector } from './app/util/mongo';
import Mongo = require('mongodb');

import cors = require('cors');
import ingredient = require('./app/router/ingredients/ingredients');

let bbtSystem = new BBTConnector("mongodb://localhost:27017", 'ingredients');
app.use(cors());


app.use(function(req, res, next){
    console.log('The request was received at ' + Date.now());
    next();
});

app.use('/ingredients', ingredient);

app.get('*',function(req, res){
    res.send("not found");
});



bbtSystem.init().then((db:Mongo.Db)=>{
    //db.collection('ingredients').remove();
    db.collection('ingredients').find().toArray((error: any, result: any)=>{
        if (error)
            console.log(error);
        if (result){
            result.map((ingredient: any, index: number)=>{
                console.log(index, ingredient);
            });
            if (result.length == 0){            
                db.collection('ingredients').save({name: 'carrot', quantity: 2})
                db.collection('ingredients').save({name: 'meat', quantity: 1})
                db.collection('ingredients').save({name: 'potato', quantity: 1})
                db.collection('ingredients').save({name: 'rice', quantity: 1})
                db.collection('ingredients').save({name: 'garlic', quantity: 1})
                db.collection('ingredients').save({name: 'onion', quantity: 1})
                db.collection('ingredients').save({name: 'milk', quantity: 1})
                db.collection('ingredients').save({name: 'bread', quantity: 1})
                db.collection('ingredients').find().toArray((error: any, result: any)=>{
                    if (error)
                    console.log(error);
                if (result){
                    result.map((ingredient: any, index: number)=>{
                        console.log(index, ingredient);
                        });
                    }
                });
            }
        }
    });
});
app.listen(3000);