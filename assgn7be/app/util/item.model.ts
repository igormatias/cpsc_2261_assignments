export class Ingredient {
    constructor(public name: string, public quantity: number) { }
}
export class Recipe{
    constructor(public name: string, public ingredients: Ingredient[], public instructions: string[], public estimatedTime: string) { }
}

export class Fridge{
    constructor(public ingredients: Ingredient[]) { }
}