"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Mongo = require("mongodb");
var mc = Mongo.MongoClient; //Short name of mongo client
var BBTConnector = /** @class */ (function () {
    function BBTConnector(connectionString, dbName) {
        this.connectionString = connectionString;
        this.dbName = dbName;
    }
    BBTConnector.prototype.init = function () {
        var _this = this;
        this.database = this.database || new Promise(function (resolve, reject) {
            //resolve and reject are functions
            mc.connect(_this.connectionString, function (err, client) {
                if (!err) {
                    console.log("Database was connected successfully");
                    //console.log(client.db(this.dbName));
                    resolve(client.db(_this.dbName));
                }
                else {
                    reject(err);
                    console.log("Error connecting");
                }
            });
        });
        return this.database;
    };
    return BBTConnector;
}());
exports.BBTConnector = BBTConnector;
