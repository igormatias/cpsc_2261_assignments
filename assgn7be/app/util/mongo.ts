import Mongo = require('mongodb');

const mc = Mongo.MongoClient; //Short name of mongo client


export class BBTConnector {
    private database: Promise<Mongo.Db>;              //deliberately not initializing my variables here


    constructor(public connectionString: string, public dbName: string) { }
    
    init(): Promise<any>{
        this.database = this.database ||  new Promise<Mongo.Db>((resolve: any, reject: any) => {  //Should use types, but gets annoying figuring the right one with promise.
            //resolve and reject are functions
            mc.connect(this.connectionString, (err: any, client: any) => {
                if (!err) {
                    console.log("Database was connected successfully");
                    //console.log(client.db(this.dbName));
                    resolve(client.db(this.dbName));
                } else {
                    reject(err);
                    console.log("Error connecting");
                }
            })
        });
        return this.database;
    }
}