"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var app = require("./config/config");
var mongo_1 = require("./app/util/mongo");
var cors = require("cors");
var ingredient = require("./app/router/ingredients/ingredients");
var bbtSystem = new mongo_1.BBTConnector("mongodb://localhost:27017", 'ingredients');
app.use(cors());
app.use(function (req, res, next) {
    console.log('The request was received at ' + Date.now());
    next();
});
app.use('/ingredients', ingredient);
app.get('*', function (req, res) {
    res.send("not found");
});
bbtSystem.init().then(function (db) {
    //db.collection('ingredients').remove();
    db.collection('ingredients').find().toArray(function (error, result) {
        if (error)
            console.log(error);
        if (result) {
            result.map(function (ingredient, index) {
                console.log(index, ingredient);
            });
            if (result.length == 0) {
                db.collection('ingredients').save({ name: 'carrot', quantity: 2 });
                db.collection('ingredients').save({ name: 'meat', quantity: 1 });
                db.collection('ingredients').save({ name: 'potato', quantity: 1 });
                db.collection('ingredients').save({ name: 'rice', quantity: 1 });
                db.collection('ingredients').save({ name: 'garlic', quantity: 1 });
                db.collection('ingredients').save({ name: 'onion', quantity: 1 });
                db.collection('ingredients').save({ name: 'milk', quantity: 1 });
                db.collection('ingredients').save({ name: 'bread', quantity: 1 });
                db.collection('ingredients').find().toArray(function (error, result) {
                    if (error)
                        console.log(error);
                    if (result) {
                        result.map(function (ingredient, index) {
                            console.log(index, ingredient);
                        });
                    }
                });
            }
        }
    });
});
app.listen(3000);
