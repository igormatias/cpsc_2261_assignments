mongodb:

examples:

use fridge_management
show dbs
db.alunos.save({name: "jorge"})

Creating a Collection:

db.createCollection("collectionName")

Seeing the collections:
db.getCollectionNames()

drop a collection:
db.collectionName.drop()

Inserting a document:
db.collectionName.save({name: "test", age: 20, sex: "male", courses: [{name: "Web Technology", code: "CPSC2261"}, {name: "Web Development II", code: "CPSC2030"}]})

showing the first register:
db.collectionName.findOne()

showing register:
db.collectionName.find()




db.collectionName.find({name:{$eq : "test"}}).pretty()
db.collectionName.find({age:{$lt : 30}})
db.collectionName.find({sex:{$nq : "male"}})
db.collectionName.find({name:{$eq : "test"}},{age:{eq:30}})
db.collectionName.find($or:[{nome:{$eq:"maria"}}, {nome:{$eq: "jose"}}])


$eq - equal
$gt - greater than
$gte - greater than or equal
$lt - less than
$ltr - less than or equal
$ne - not equal

//only updates when exists
db.collectionName.update({name:"test"},{$set:{sex: 'F'},{age: 25}},{multi: true})

//updates if exist or create a new register (to update we need to use _id from mongodb)
db.collectionName.save()


// justOne - second parameter, true or false
db.collectionName.remove({name: {$eq: "test"}}, true)



