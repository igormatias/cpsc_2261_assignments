"use strict";
var Mongo = require('mongodb');
var mc = Mongo.MongoClient;
var database;
var dbConnection = function () {
    console.log('mongoDB connection');
    this.database = this.database || new Promise(function (resolve, reject) {
        mc.connect('mongodb://localhost:27017', function (err, client) {
            if (!err) {
                console.log("Database was connected successfully");
                //console.log(client.db(this.dbName));
                resolve(client.db('frigeMg'));
            }
            else {
                reject(err);
                console.log("Error connecting");
            }
        });
    });
    return this.database;
};
module.exports = function () {
    return dbConnection;
};
