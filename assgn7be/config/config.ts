let express = require('express');
let consign = require('consign');
let bodyParser = require('body-parser');
let cors = require('cors');

let app = express();

app.use(bodyParser());
app.use(cors());

consign()
    .include('routes/test.js')
    .then('routes/ingredients.js')
    .then('config/dbConnection.js')
    .then('models/testModel.js')
    .then('models/ingredientsDAO.js')
    .into(app);

export = app;
