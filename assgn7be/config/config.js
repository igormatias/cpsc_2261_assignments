"use strict";
var express = require('express');
var consign = require('consign');
var bodyParser = require('body-parser');
var cors = require('cors');
var app = express();
app.use(bodyParser());
app.use(cors());
consign()
    .include('routes/test.js')
    .then('routes/ingredients.js')
    .then('config/dbConnection.js')
    .then('models/testModel.js')
    .then('models/ingredientsDAO.js')
    .into(app);
module.exports = app;
