let Mongo = require('mongodb');

let mc = Mongo.MongoClient;
let database: Promise<Mongo.Db>;

let dbConnection = function () {
    console.log('mongoDB connection');
    this.database = this.database || new Promise<Mongo.Db>((resolve: any, reject: any) => {
        mc.connect('mongodb://localhost:27017', (err: any, client: any) => {
            if (!err) {
                console.log("Database was connected successfully");
                //console.log(client.db(this.dbName));
                resolve(client.db('frigeMg'));
            } else {
                reject(err);
                console.log("Error connecting");
            }
        });
    });
    return this.database; 
}
export = () => {
    return dbConnection;
};