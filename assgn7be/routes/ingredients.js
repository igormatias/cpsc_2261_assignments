"use strict";
var Result = /** @class */ (function () {
    function Result() {
        this.found = false;
        this.index = 0;
    }
    return Result;
}());
var Ingredient = /** @class */ (function () {
    function Ingredient(name, quantity) {
        this.name = name;
        this.quantity = quantity;
    }
    return Ingredient;
}());
var Util = /** @class */ (function () {
    function Util() {
    }
    Util.prototype.checkIfExist = function (ingredient, ingredients) {
        var found = false;
        var index = -1;
        for (var i = 0; i < ingredients.length && !found; i++) {
            if (ingredients[i].name == ingredient.name) {
                index = i;
                found = true;
            }
        }
        return { found: found, index: index };
    };
    Util.prototype.addIngredientToList = function (ingredient, list, result) {
        if (result.found)
            list[result.index].quantity += ingredient.quantity;
        else
            list.push(ingredient);
    };
    Util.prototype.removeIngredient = function (ingredient, list, result) {
        if (result.found) {
            list[result.index].quantity -= ingredient.quantity;
            if (list[result.index].quantity <= 0)
                list.splice(result.index, 1);
        }
    };
    Util.prototype.copyIngredients = function (ingredients) {
        var newIngredients = [];
        ingredients.map(function (ingredient) {
            newIngredients.push(new Ingredient(ingredient.name, ingredient.quantity));
        });
        return newIngredients;
    };
    Util.prototype.copyInstructions = function (instructions) {
        var newInstructions = [];
        instructions.map(function (instruction) {
            newInstructions.push(instruction);
        });
        return newInstructions;
    };
    return Util;
}());
module.exports = function (app) {
    var ingredientsList;
    app.get('/ingredients/test', function (req, res) {
        res.send('test ok');
    });
    app.get('/ingredients/getIngredients', function (req, res) {
        res.header("Content-Type", "application/json");
        var list = app.models.ingredientsDAO.getData();
        list.then(function (answer, error) {
            if (!error) {
                console.log(answer);
                ingredientsList = answer;
                res.send(ingredientsList);
            }
            else {
                res.send({ error: 1 });
            }
        });
    });
    app.post('/ingredients/addIngredient', function (req, res) {
        var util = new Util();
        res.header("Content-Type", "application/json");
        var ingredient = req.body;
        //ingredient = { name: 'test', quantity: 1 };
        var result = util.checkIfExist(ingredient, ingredientsList);
        util.addIngredientToList(ingredient, ingredientsList, result);
        if (!result.found)
            app.models.ingredientsDAO.addData(ingredient);
        else {
            app.models.ingredientsDAO.updateData(ingredientsList[result.index]);
        }
        res.send(ingredientsList);
    });
    app.post('/ingredients/removeIngredient', function (req, res) {
        var util = new Util();
        res.header("Content-Type", "application/json");
        var ingredient = req.body;
        //ingredient = { name: 'test', quantity: 1 };
        var result = util.checkIfExist(ingredient, ingredientsList);
        if (result.found) {
            app.models.ingredientsDAO.updateData(ingredientsList[result.index]);
            if (ingredientsList[result.index].quantity <= 0)
                app.models.ingredientsDAO.removeData(ingredient);
        }
        util.removeIngredient(ingredient, ingredientsList, result);
        res.send(ingredientsList);
    });
};
