"use strict";
module.exports = function (app) {
    app.get('/test', function (req, res) {
        app.config.dbConnection();
        var testModel = app.models.testModel;
        testModel.getData();
        res.send('test ok');
    });
    app.post('/test/post', function (req, res) {
        var answer = req.body;
        res.send([answer, { test: 1 }]);
    });
};
