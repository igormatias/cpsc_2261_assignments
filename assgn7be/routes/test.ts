export = (app: any) =>{
    app.get('/test', (req: any, res: any)=>{
        app.config.dbConnection();
        let testModel = app.models.testModel;
        testModel.getData();
        res.send('test ok');
    });
    app.post('/test/post', (req: any, res: any)=>{
        let answer = req.body;
        res.send([answer, {test: 1}]);
    });

};