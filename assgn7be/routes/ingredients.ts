class Result{
    public found: boolean = false;
    public index: number = 0;
}

class Ingredient {
    constructor(public name: string, public quantity: number) { }
}

class Util{
    checkIfExist(ingredient: Ingredient, ingredients: Ingredient[]): Result{
        let found: boolean = false;
        let index: number = -1;
        for (let i: number = 0; i < ingredients.length && !found; i++){
            if (ingredients[i].name == ingredient.name){
                index = i;
                found = true;
            }
        }
        return {found, index};
    }

    addIngredientToList(ingredient: Ingredient, list: Ingredient[], result: Result){
        if (result.found)
            list[result.index].quantity += ingredient.quantity;
        else
            list.push(ingredient);
    }

    removeIngredient(ingredient: Ingredient, list: Ingredient[], result: Result){
        if (result.found){
            list[result.index].quantity -= ingredient.quantity;
        if (list[result.index].quantity <= 0)
            list.splice(result.index, 1);
        }
    }
    copyIngredients(ingredients: Ingredient[]): Ingredient[]{
        let newIngredients: Ingredient[] = [];
        ingredients.map( (ingredient: Ingredient) =>{
            newIngredients.push(new Ingredient(ingredient.name, ingredient.quantity));
        })
        return newIngredients;
    }

    copyInstructions(instructions: string[]): string[]{
        let newInstructions: string[] = [];
        instructions.map( (instruction: string) =>{
            newInstructions.push(instruction);
        })
        return newInstructions;
    }
}


export = (app: any) =>{
    let ingredientsList: Ingredient[];
    
    app.get('/ingredients/test', (req: any, res: any)=>{
        res.send('test ok');
    });

    app.get('/ingredients/getIngredients', (req: any, res: any)=>{
        res.header("Content-Type","application/json");
        let list = app.models.ingredientsDAO.getData();
        list.then((answer: Ingredient[], error: any)=>{
            if (!error){
                console.log(answer);
                ingredientsList = answer;
                res.send(ingredientsList);
            }
            else{
                res.send({error: 1});
            }
        });
    });

    app.post('/ingredients/addIngredient', (req: any, res: any)=>{
        let util = new Util();
        res.header("Content-Type","application/json");
        let ingredient = req.body;
        //ingredient = { name: 'test', quantity: 1 };
        let result = util.checkIfExist(ingredient, ingredientsList);
        util.addIngredientToList(ingredient, ingredientsList, result);
        if (!result.found)
            app.models.ingredientsDAO.addData(ingredient);
        else{
            app.models.ingredientsDAO.updateData(ingredientsList[result.index]);
        }
        res.send(ingredientsList);
    });

    app.post('/ingredients/removeIngredient', function(req, res){
        let util = new Util();
        res.header("Content-Type","application/json");
        let ingredient = req.body;
        //ingredient = { name: 'test', quantity: 1 };
        let result: Result = util.checkIfExist(ingredient, ingredientsList);
        if (result.found){
            app.models.ingredientsDAO.updateData(ingredientsList[result.index]);
        if (ingredientsList[result.index].quantity <= 0)
        app.models.ingredientsDAO.removeData(ingredient);
        }
        util.removeIngredient(ingredient, ingredientsList, result);
        res.send(ingredientsList);
    });
    
};