export = (app) => {
    function connect() {
        let db = app.config.dbConnection();
        return db;

    }

    this.getData = () => {
        let ingredientsList: Promise = new Promise((resolve: any, reject: any) => {
            connect().then((db, error) => {
                if (!error) {
                    //db.collection('ingredients').remove();
                    db.collection('ingredients').find().toArray((error: any, result: any) => {
                        if (error) {
                            console.log(error);
                            reject(error);
                        }
                        if (result) {
                            if (result.length == 0) {
                                db.collection('ingredients').save({ name: 'carrot', quantity: 2 })
                                db.collection('ingredients').save({ name: 'meat', quantity: 1 })
                                db.collection('ingredients').save({ name: 'potato', quantity: 1 })
                                db.collection('ingredients').save({ name: 'rice', quantity: 1 })
                                db.collection('ingredients').save({ name: 'garlic', quantity: 1 })
                                db.collection('ingredients').save({ name: 'onion', quantity: 1 })
                                db.collection('ingredients').save({ name: 'milk', quantity: 1 })
                                db.collection('ingredients').save({ name: 'bread', quantity: 1 })
                                db.collection('ingredients').find().toArray((error: any, result: any) => {
                                    if (error) {
                                        console.log(error);
                                        reject(error);
                                    }
                                    if (result) {
                                        resolve(result);
                                    }
                                });
                            }
                            else {
                                resolve(result);
                            }    
                        }
                    });
                }
            });
        });
        return ingredientsList;
    }

    this.addData = (ingredient: any) => {
        connect().then((db, error) => {
            if (!error) {
                db.collection('ingredients').save(ingredient);
            }
        });
    }

    this.updateData = (ingredient: any) => {
        connect().then((db, error) => {
            if (!error) {
                db.collection('ingredients').update({name: ingredient.name}, {$set: {quantity: ingredient.quantity}});
            }
        });
    }

    this.removeData = (ingredient: any) => {
        connect().then((db, error) => {
            if (!error) {
                console.log(ingredient);
                db.collection('ingredients').remove({name: {$eq: ingredient.name}}, true);
            }
        });
    }



    return this;
}