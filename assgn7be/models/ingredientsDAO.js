"use strict";
var _this = this;
module.exports = function (app) {
    function connect() {
        var db = app.config.dbConnection();
        return db;
    }
    _this.getData = function () {
        var ingredientsList = new Promise(function (resolve, reject) {
            connect().then(function (db, error) {
                if (!error) {
                    //db.collection('ingredients').remove();
                    db.collection('ingredients').find().toArray(function (error, result) {
                        if (error) {
                            console.log(error);
                            reject(error);
                        }
                        if (result) {
                            if (result.length == 0) {
                                db.collection('ingredients').save({ name: 'carrot', quantity: 2 });
                                db.collection('ingredients').save({ name: 'meat', quantity: 1 });
                                db.collection('ingredients').save({ name: 'potato', quantity: 1 });
                                db.collection('ingredients').save({ name: 'rice', quantity: 1 });
                                db.collection('ingredients').save({ name: 'garlic', quantity: 1 });
                                db.collection('ingredients').save({ name: 'onion', quantity: 1 });
                                db.collection('ingredients').save({ name: 'milk', quantity: 1 });
                                db.collection('ingredients').save({ name: 'bread', quantity: 1 });
                                db.collection('ingredients').find().toArray(function (error, result) {
                                    if (error) {
                                        console.log(error);
                                        reject(error);
                                    }
                                    if (result) {
                                        resolve(result);
                                    }
                                });
                            }
                            else {
                                resolve(result);
                            }
                        }
                    });
                }
            });
        });
        return ingredientsList;
    };
    _this.addData = function (ingredient) {
        connect().then(function (db, error) {
            if (!error) {
                db.collection('ingredients').save(ingredient);
            }
        });
    };
    _this.updateData = function (ingredient) {
        connect().then(function (db, error) {
            if (!error) {
                db.collection('ingredients').update({ name: ingredient.name }, { $set: { quantity: ingredient.quantity } });
            }
        });
    };
    _this.removeData = function (ingredient) {
        connect().then(function (db, error) {
            if (!error) {
                console.log(ingredient);
                db.collection('ingredients').remove({ name: { $eq: ingredient.name } }, true);
            }
        });
    };
    return _this;
};
