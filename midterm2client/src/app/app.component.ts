import { Component } from '@angular/core';
import { Http, RequestOptions, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';
/*TODO: needs more refactoring 
        really it does!
        Looking at you future Kim
        from past Kim
*/
const endpoints = {
  clear: "/clear",
  create: "/create",
  list: "/list",
  map: "/map",
  move: "/move",
  fire: "/fire",
  freeze: "/freeze"
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})

export class AppComponent {
  name = 'Bot AI';
  url = 'http://localhost:3436';
  createReport: string = "";
  moveReport: Array<string> = [];
  fireReport: Array<string> = [];
  freezeMoveReport: Array<string> = [];
  freezeFireReport: Array<string> = [];

  outMessage: any = "test";
  bots: Array<any> = []; //We get the type
  constructor(private http: Http) { };
  reset(): any {
    this.bots = [];
  }
  //Test1: Bot creation Test. It's a bit Monolithic
  testCreate(): void {
    //Each time we call test, we want  new set of closure variables.
    let userPromises: any = [];
    let basename = "golf";
    let botnames: any[] = [];
    let pass = true;
    let botId: any[];
    this.bots = [];  //Dump all existing bots for the test.
    botId = []; //Somethign weird when this function is re-rantrant.
    let numBots = Math.ceil(Math.random() * 19) + 5;
    let report = (message: string) => { this.createReport = message };
    this.clearServer(report).then((response) => {
      //Ignore response 
      //Generate a random number of bots each time
      for (let i = 0; i < numBots; i += 1) {
        userPromises.push(this.http.get(this.url + endpoints.create + "/" + basename + i).toPromise()
          .then((response) => {
            this.bots.push(response.json());
            botnames.push(basename + i);
          }));
      }
      //Wait till all users gets created, then check the list
      //Two checks. 1 all the user names are there
      //none of the users have duplicate ids
      //ids are numbers
      Promise.all(userPromises).then(() => {
        console.log("Promise ALL");
        this.bots.forEach((bot) => {
          console.log(bot);
          if (this.checkObjectForProperties(bot, ["id", "name"])) {
            //Check if the botname is in the list
            if (botnames.findIndex((name) => { return (name == bot.name); }) == -1) {
              pass = false;
              this.createReport = "Can not find all bot names";
            }
            //Check if the botID is unique
            if (botId.findIndex(id => bot.id == id) != -1) {
              pass = false;
              this.createReport = "IDs not unique";
            }
            botId.push(bot.id);

          } else {
            pass = false;
            this.createReport = "One or more bot object of the wrong format.";
          }
        })
        if (pass) {
          //now get the list from the server
          this.http.get(this.url + endpoints.list).toPromise()
            .then((response: any) => {
              //For here we expect an array
              let listFromServer = response.json();
              console.log("/list");
              console.log(listFromServer);
              if (typeof (listFromServer.length) == "undefined") {
                this.createReport = "/list may not have sent an array. Check console";
              } else {
                if (listFromServer.length != numBots) {
                  this.createReport = "/list returned the wrong number of bots";
                } else {
                  this.createReport = "Bot creation passed."; //the next if statement will overwrite this.
                  listFromServer.forEach((serverBot: any) => {
                    if (this.bots.findIndex((localBot) => {
                      return localBot.id == serverBot.id && localBot.name == serverBot.name;
                    }) == -1) {
                      this.createReport = "Comparing server list to local list, missing a bot";
                    }
                  })
                }
              }
            }).catch((reason: any) => {
              this.createReport = "Server error while trying to list bots. Check console";
              console.log(reason);
            })
        }
      }).catch((reason) => {
        this.createReport = "Server Error! Check console";
        console.log(reason);
        //Check if all the bots have the correct format
        //name and ID objects
      })
    })
  }
  //Test the fire an move commands


  createTestMap() {
    let denseMap = [[1, 1, 1], [1, 0, 1], [1, 1, 1]];
    let map1 = {
      map: [[1, 0, 1], [1, 0, 1], [1, 1, 1]],
      escape: { row: 0, col: 1 }
    };
    let map2 = {
      map: [[1, 1, 1], [1, 0, 1], [1, 0, 1]],
      escape: { row: 2, col: 1 }
    };
    let map3 = {
      map: [[1, 1, 1], [0, 0, 1], [1, 1, 1]],
      escape: { row: 1, col: 0 }
    };
    let map4 = {
      map: [[1, 1, 1], [1, 0, 0], [1, 1, 1]],
      escape: { row: 1, col: 2 }
    };
    let escapableTests = [map1, map2, map3, map4];

    let map5 = {
      map: [[0, 1, 0], [0, 0, 0], [0, 0, 0]],
      escape: { row: 0, col: 1 }
    };
    let map6 = {
      map: [[0, 0, 0], [0, 0, 0], [0, 1, 0]],
      escape: { row: 2, col: 1 }
    };
    let map7 = {
      map: [[0, 0, 0], [1, 0, 0], [0, 0, 0]],
      escape: { row: 1, col: 0 }
    };
    let map8 = {
      map: [[0, 0, 0], [0, 0, 1], [0, 0, 0]],
      escape: { row: 1, col: 2 }
    };
    let killableTests = [map5, map6, map7, map8];
    return { escapeTests: escapableTests, killableTests: killableTests };
  }
  generatePostRequest(id: any, body: any, endpoint: any, report: Function) {
    let myHeaders: any = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: myHeaders });
    return this.http.post(this.url + endpoint + "/" + id, body, options).toPromise().catch((reason) => {
      report("Server Error generating post request");
      console.log(reason);
    }
    );
  }
  testMove() {
    let tests = this.createTestMap();
    this.moveReport = [];
    this.testAction(endpoints.move, tests.escapeTests, (message: string) => { this.moveReport.push(message); });
  }
  testFire() {
    let tests = this.createTestMap();
    this.fireReport = [];
    this.testAction(endpoints.fire, tests.killableTests, (message: string) => { this.fireReport.push(message)});
  }
  clearServer(report: Function) {
    return this.http.get(this.url + endpoints.clear).toPromise().catch((reason) => {
      report("Error Clearing Server, check console");
      console.log(reason);
    });
  }
  createBots(basename: string, numBots: number, report: Function) {
    let botPromises: any[] = [];
    for (let i = 0; i < numBots; i += 1) {
      botPromises.push(this.http.get(this.url + endpoints.create + "/name" + basename + i).toPromise().catch((reason) => {
        report("Server error while creating bot:" + i + "please check console");
        console.log(reason);
      }))
    }
    return botPromises;
  }
  testAction(action: any, testset: any, report: Function) {
    let basename = "tango";
    let numBots = 20;
    let botPromises: any[] = [];
    let offset = Math.ceil(Math.random() * 3)
    //step 1 clear the list, and create multiple bots
    this.clearServer(report).then((response) => {
      botPromises = this.createBots(basename, numBots, report);
      //Launch each test on a different bot
      for (let i = 0; i < testset.length; i += 1) {
        botPromises[i + offset].then((response: any) => {
          let bot = response.json();
          let testObj = testset[i];
          //Send the test map
          this.generatePostRequest(bot.id, testObj.map, endpoints.map, report).then((response: any) => {
            let resObj = response.json();
            if (this.checkObjectForProperties(resObj, ["endpoint", "echo"])) {
              if (resObj.endpoint != "/map" || JSON.stringify(resObj.echo) != JSON.stringify(testset[i].map)) {
                report("Error in response to /map");
                console.log("error");
              } else {
                //now send the action request
                this.generatePostRequest(bot.id, {}, action, report).then((response: any) => {
                  report(i + ":Finally testing what we want. \n");
                  let obj = response.json();
                  if (this.checkObjectForProperties(obj, ["row", "col"])) {
                    //contains property
                    report("Has properties");
                    if (obj.row == testObj.escape.row && obj.col == testObj.escape.col) {
                      report("Test passed");
                    } else {
                      report("Wrong Values");
                    }
                  } else {
                    report("Missing properties in returned object")
                  }
                  console.log("From Server");
                  console.log(response.json());
                  console.log("Expected");
                  console.log(testObj.escape);

                })
              }
            } else {
              report("Wrong response type");
              console.log(response.json());
            }
          })
        })
      }
    })
  }
  //testing if commands are rejected for 2 seconds

  testFrozenAction(action: any, bot: any, testMap: any, report: Function) {
    bot.then((response: any) => {
      let bot = response.json();
      //load in the map
      console.log(bot); //So we know which bot we're expecting
      this.generatePostRequest(bot.id, testMap, endpoints.map, report).then((response: any) => {
        //Assuming map worked. Previous tests 
        this.generatePostRequest(bot.id, {}, endpoints.freeze, report).then((response: any) => {
          //Check response
          console.log(response);
          let obj = response.json();
          if (this.checkObjectForProperties(obj, ["message"])) {
            if (obj.message == "frozen") {
              console.log("Correct frozen response, attempting move command");
              this.generatePostRequest(bot.id, {}, action, report).then((response: any) => {
                console.log(response);
                let obj = response.json();
                if (this.checkObjectForProperties(obj, ["error"])) {
                  if (obj.error == "frozen") {
                    console.log("frozen detect. Moving on to check thawing");
                    report("Beginning 2 second wait.")
                    setTimeout(() => {
                      this.generatePostRequest(bot.id, {}, action, report).then((response: any) => {
                        console.log(response);
                        obj = response.json();
                        if (this.checkObjectForProperties(obj, ["row", "col"])) {
                          report("Unfreezing successful");
                        } else {
                          report("Error in respose, could still be frozen");
                        }
                      })
                    }, 2100);//
                  } else {
                    report("Expecting error value frozen but got somethign else.check console")
                  }
                } else {
                  report("/move expecting error response due to freezing, but got something else.Check console");
                }
              });
            } else {
              report("/freee message value incorrect");
            }
          } else {
            report("Error in /freeze response. expecting message");

          }
        })
      })
    })

  }
  testFreeze() {
    let reportMove = (message: string) => { this.freezeMoveReport.push(message) };
    let reportFire = (message: string) => { this.freezeFireReport.push( message) };
    this.clearServer(reportMove).then((reason) => {
      let numbots = 10;
      let basename = "freezer";
      let tests = this.createTestMap();
      let botPromises = this.createBots(basename, numbots, reportMove);
      let randomBot = Math.floor(Math.random() * (numbots - 1));
      let testMap = tests.escapeTests[0].map;
      this.testFrozenAction(endpoints.move, botPromises[randomBot], testMap, reportMove);
      this.testFrozenAction(endpoints.fire, botPromises[randomBot + 1], testMap, reportFire);

    })
  }
  testError() {
    let report = (message: string) => { console.log(message) };
    this.clearServer(report).then((response) => {
      //Ignoring response
      let endpointTests = [endpoints.map, endpoints.fire, endpoints.move];
      for (let i = 0; i < endpointTests.length; i += 1) {
        let ep = endpointTests[i];
        this.generatePostRequest(100, { test: "random" }, ep, report).then((response: any) => {
          console.log(response);
          let obj = response.json();
          if (this.checkObjectForProperties(obj, ["error"])) {
            console.log("Got expected error message for : " + ep + " " + obj.error);
          } else {
            console.log("For " + ep + " Expected error");
          }
        });
      }
    })
  }
  checkObjectForProperties(obj: any, prop: any[]) {
    let missingProp = false;
    for (let i = 0; i < prop.length; i += 1) {
      if (typeof (obj[prop[i]]) === "undefined") {
        missingProp = true;
      }
    }
    return !missingProp;
  }
}
