import { Midterm2clientPage } from './app.po';

describe('midterm2client App', () => {
  let page: Midterm2clientPage;

  beforeEach(() => {
    page = new Midterm2clientPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
