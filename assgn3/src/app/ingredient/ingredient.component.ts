import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import Ingredient from './ingredient.class';

@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.css']
})
export class IngredientComponent implements OnInit {
  ingredients: Ingredient[] = [];
  ingName: string = '';
  ingQty: number;
  button: {value: string, class: string, index: number} = {value: "Add", class: "fa fa-check", index: 0};
  
  @Output() sendToRecipe = new EventEmitter();

  constructor() {
    this.add({name:'carrot',quantity: 1});
    this.add({name:'meat',quantity: 1});
    this.add({name:'potato',quantity: 1});
    this.add({name:'rice',quantity: 1});
    this.add({name:'garlic',quantity: 1});
    this.add({name:'onion',quantity: 1});
    
  }
  private add(ingredient : Ingredient){
    this.ingredients.push(ingredient);
  }

  private checkIfExist(ingredient: Ingredient): {found: boolean, index: number}{
    let found: boolean = false;
    let index: number = -1;
    for (let i: number = 0; i < this.ingredients.length && !found; i++){
        if (this.ingredients[i].name == ingredient.name){
            index = i;
            found = true;
        }
    }
    return {found, index};
}

  addIng(index: number, quantity: number){
    this.ingredients[index].quantity += quantity;
  }
  
  removeIng(index: number){
    this.ingredients[index].quantity--;
    if (this.ingredients[index].quantity == 0)
      this.removeAll(index);
  }

  removeAll(index: number){
    this.ingredients.splice(index, 1);
  }

  addNewIng(){
    if (this.button.value == 'Add'){
      let ing = new Ingredient(this.ingName, Number(this.ingQty));
      let result: {found: boolean, index: number} = this.checkIfExist(ing);
      if (result.found)
        this.addIng(result.index, ing.quantity);
      else
        this.add(ing);
    }
    else{
      this.button.value = 'Add';
      this.button.class = 'fa fa-check';
      this.ingredients[this.button.index].name = this.ingName;
      this.ingredients[this.button.index].quantity = this.ingQty;
    }
    this.ingName = '';
    this.ingQty = 0;
  }

  editIng(index: number){
    this.ingName = this.ingredients[index].name;
    this.ingQty = this.ingredients[index].quantity;
    this.button.value = " Edit"
    this.button.class = "fa fa-pencil";
    this.button.index = index;
  }

  addToRecipe(index: number){
    this.sendToRecipe.emit(this.ingredients[index]);
  }

  ngOnInit() {
  }

}
