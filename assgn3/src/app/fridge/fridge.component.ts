import { Component, OnInit, Output} from '@angular/core';
import Fridge from './fridge.class';
import Recipe from '../recipe/recipe.class';
import Ingredient from '../ingredient/ingredient.class';

@Component({
  selector: 'app-fridge',
  templateUrl: './fridge.component.html',
  styleUrls: ['./fridge.component.css']
})
export class FridgeComponent implements OnInit {
  fridge =  new Fridge();
  recipe = new Recipe([], [], '');
  list: {shoppingList: Ingredient[], fridgeIngredients: Ingredient[]};


  constructor() {
    this.list = this.fridge.checkRecipe(this.recipe);
  }

  ngOnInit() {
    
  }

  updateLists(){
    this.list = this.fridge.checkRecipe(this.recipe);
  }

  private checkIngInShoppingList(ingredient: Ingredient): number{
    for (let i: number = 0; i < this.list.shoppingList.length; i++){
      if (ingredient.name == this.list.shoppingList[i].name)
        return this.list.shoppingList[i].quantity;
    }
    return 0;
  }

  addIng(ingredient: Ingredient){
    let ing: Ingredient = new Ingredient(ingredient.name, 1);
    this.fridge.add(ing);
    this.updateLists();
  }
  
  removeIng(ingredient: Ingredient){
    let ing: Ingredient = new Ingredient(ingredient.name, 1);
    this.fridge.remove(ing);
    this.updateLists();
  }

  removeAll(ingredient: Ingredient){
    this.fridge.remove(ingredient);
    this.updateLists();
  }

  addFromShoppingList(ingredient: Ingredient){
    let ing: Ingredient = new Ingredient(ingredient.name, ingredient.quantity);    
    this.fridge.add(ing);
    this.updateLists();
  }

  checkIfMissing(ingredient: Ingredient):string{
    let quantity: number = this.checkIngInShoppingList(ingredient);
    if (quantity <= 0)
      return 'ok';
    return 'missing ' + quantity;
  }

  getFromRecipe(ingredient: Ingredient){
    this.addFromShoppingList(ingredient);
  }
  removeOfRecipe(index: number){
    this.recipe.ingredients.splice(index, 1);
    this.updateLists();
  }
}
