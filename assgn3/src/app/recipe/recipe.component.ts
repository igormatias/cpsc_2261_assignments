import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import Recipe from './recipe.class';
import Ingredient from '../ingredient/ingredient.class';

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.css']
})


export class RecipeComponent implements OnInit {
  recipe = new Recipe([], [], '');
  instructionInput: string;
  button: {value: string, class: string, index: number} = {value: "Add", class: "fa fa-check", index: 0};
  minutes = this.recipe.estimatedTime;
  
  @Output() sendToFridge = new EventEmitter();
  @Output() updateFridge = new EventEmitter();

  constructor() {

  }


  private add(ingredient : Ingredient){
    this.recipe.addItem(ingredient);
  }

  getFromMarket(ingredient: Ingredient){
    this.add(ingredient);
    this.update(this.recipe.ingredients);
  }
  
  addIng(index: number, quantity: number){
    this.recipe.ingredients[index].quantity += quantity;
    this.update(this.recipe.ingredients);
  }
  
  removeIng(index: number){
    this.recipe.ingredients[index].quantity--;
    if (this.recipe.ingredients[index].quantity == 0)
      this.removeAll(index);
    else
      this.update(this.recipe.ingredients);
  }

  removeAll(index: number){
    this.recipe.ingredients.splice(index, 1);
    this.update(this.recipe.ingredients);
  }

  editInstr(index: number){
    this.instructionInput = this.recipe.instructions[index];
    this.button.value = " Edit"
    this.button.class = "fa fa-pencil";
    this.button.index = index;
  }

  removeInstr(index: number){
    this.recipe.instructions.splice(index, 1);
  }

  addNewInstruction(){
    if (this.button.value == "Add")
      this.recipe.instructions.push(this.instructionInput);
    else{
      this.button.value = "Add";
      this.button.class = "fa fa-check";
      this.recipe.instructions[this.button.index] = this.instructionInput;
    }
    this.instructionInput = "";  
  }

  addToFridge(index: number){
    this.sendToFridge.emit(this.recipe.ingredients[index]);
  }

  update(ingredients: Ingredient[]){
    this.updateFridge.emit(ingredients);
  }

  ngOnInit() {
    

    
  }
}
