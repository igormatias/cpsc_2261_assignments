import express = require('express');
import { Ingredient } from './item.model';
import bodyParser = require('body-parser');
import { Util } from './util.class';
import { Result } from './util.class';

let router = express.Router();
router.use(bodyParser.json()); //Parse json http bodies
let ingredientsList : Ingredient[];
let util = new Util();


ingredientsList = [
    new Ingredient('carrot', 1), 
    new Ingredient('meat', 1), 
    new Ingredient('potato', 1), 
    new Ingredient('rice', 1), 
    new Ingredient('garlic', 1), 
    new Ingredient('onion', 1), 
    new Ingredient('milk', 1),
];


router.get('/getIngredients', function(req, res){
    res.header("Content-Type","application/json");
    res.json(ingredientsList);
});

router.get('/test', function(req, res){
    res.send('{"test": 1 }');
});

router.post('/addIngredient', function(req, res){
    let ingredient = req.body;
    console.log(ingredient);
    let result: Result = util.checkIfExist(ingredient, ingredientsList);
    util.addIngredientToList(ingredient, ingredientsList, result);
    res.send(ingredientsList);
});

router.post('/removeIngredient', function(req, res){
    let ingredient = req.body;
    let result: Result = util.checkIfExist(ingredient, ingredientsList);
    util.removeIngredient(ingredient, ingredientsList, result);
    res.send(ingredientsList);
});

router.post('/editIngredient', function(req, res){
    ingredientsList[req.body['index']] = req.body['ingredient'];
    res.send(ingredientsList);
});

//module.exports = router;
export = router;