import express = require('express');
import cors = require('cors');
import bodyParser = require('body-parser');
import { Ingredient } from './item.model';
import ingredient = require('./ingredients');

let app = express();


app.use(cors());


app.use(function(req, res, next){
    console.log('The request was received at ' + Date.now());
    next();
});

app.use('/ingredients', ingredient);
     
app.get('*',function(req, res){
    res.send("not found");
});

app.listen(3000);