import { Ingredient } from './item.model';

export class Result{
    public found: boolean = false;
    public index: number = 0;
}

export class Util{
    checkIfExist(ingredient: Ingredient, ingredients: Ingredient[]): Result{
        let found: boolean = false;
        let index: number = -1;
        for (let i: number = 0; i < ingredients.length && !found; i++){
            if (ingredients[i].name == ingredient.name){
                index = i;
                found = true;
            }
        }
        return {found, index};
    }

    addIngredientToList(ingredient: Ingredient, list: Ingredient[], result: Result){
        if (result.found)
            list[result.index].quantity += ingredient.quantity;
        else
            list.push(ingredient);
    }

    removeIngredient(ingredient: Ingredient, list: Ingredient[], result: Result){
        if (result.found)
            list[result.index].quantity -= ingredient.quantity;
        if (list[result.index].quantity <= 0)
            list.splice(result.index, 1);
    }
    copyIngredients(ingredients: Ingredient[]): Ingredient[]{
        let newIngredients: Ingredient[] = [];
        ingredients.map( (ingredient: Ingredient) =>{
            newIngredients.push(new Ingredient(ingredient.name, ingredient.quantity));
        })
        return newIngredients;
    }

    copyInstructions(instructions: string[]): string[]{
        let newInstructions: string[] = [];
        instructions.map( (instruction: string) =>{
            newInstructions.push(instruction);
        })
        return newInstructions;
    }
}