"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var cors = require("cors");
var ingredient = require("./ingredients");
var app = express();
app.use(cors());
app.use(function (req, res, next) {
    console.log('The request was received at ' + Date.now());
    next();
});
app.use('/ingredients', ingredient);
app.get('*', function (req, res) {
    res.send("not found");
});
app.listen(3000);
