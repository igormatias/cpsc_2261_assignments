var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');

var app = express();
app.use(cors());

app.use(function(req, res, next){
    console.log('The request was received at ' + Date.now());
    next();
});

var things = require('./ingredients');
app.use('/ingredients', things);
    
app.use('/', function(req, res){
    console.log('End');
 });
 
app.get('*',function(req, res){
    res.send("not found");
});

app.listen(3000);