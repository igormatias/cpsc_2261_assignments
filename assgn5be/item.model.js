"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Ingredient = /** @class */ (function () {
    function Ingredient(name, quantity) {
        this.name = name;
        this.quantity = quantity;
    }
    return Ingredient;
}());
exports.Ingredient = Ingredient;
var Recipe = /** @class */ (function () {
    function Recipe(name, ingredients, instructions, estimatedTime) {
        this.name = name;
        this.ingredients = ingredients;
        this.instructions = instructions;
        this.estimatedTime = estimatedTime;
    }
    return Recipe;
}());
exports.Recipe = Recipe;
var Fridge = /** @class */ (function () {
    function Fridge(ingredients) {
        this.ingredients = ingredients;
    }
    return Fridge;
}());
exports.Fridge = Fridge;
