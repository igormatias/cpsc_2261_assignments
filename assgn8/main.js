const image = new Image();
image.src = 'image.jpg';

image.addEventListener('load', ()=>{
    let canvas = document.getElementById('image');
    let sliderBlur = document.getElementById('blur');
    let sliderTime = document.getElementById('time');
    let ctx = canvas.getContext("2d");
    let imageData;
    let command;
    
    initConfig = ()=>{
            
        canvas.width = image.width;
        canvas.height = image.height;
        canvas.style = 'border: 1px solid black';
        ctx.drawImage(image, 0, 0);
        imageData = ctx.getImageData(0, 0, image.width, image.height);
    }
    
    if (window.Worker){
        initConfig();
        command = {command: 'loadImage', image: imageData}
        let myWorker = new Worker('worker.js');
        myWorker.postMessage(command);
        command = {command: 'start'}
        myWorker.postMessage(command);
    
        sliderBlur.addEventListener('change', (e)=>{
            console.log(e.target.value);
            command = {command: 'setBlur', value: e.target.value/100}
            myWorker.postMessage(command);
        });    
    
        sliderTime.addEventListener('change', (e)=>{
            console.log(e.target.value);
            command = {command: 'setTime', value: e.target.value * 10}
            myWorker.postMessage(command);
        });    
        myWorker.onmessage = (e)=>{
            ctx.putImageData(e.data, 0, 0);
        }
    }
});


