let imageData;
let count = 0;
let a = 0.5;
let deltax = 1;
let deltay = 1;
let deltatime = 1;
let time = 1000;
let intervalID;

onmessage = (e) =>{
    console.log(e.data.command);
    if (e.data.command == 'loadImage'){
        imageData = e.data.image;
        console.log(imageData)
    }
    if (e.data.command == 'setBlur')
        a = e.data.value;
    if (e.data.command == 'setTime'){
        time = e.data.value;
        clearInterval(intervalID);
        intervalID = setInterval(imageBlur, time);
    }
    if (e.data.command == 'start'){
        console.log('start to work');
        intervalID = setInterval(imageBlur, time);
        imageBlur();
    }
    
    //postMessage(imageData);

}


imageBlur = () =>{
    console.log('imageBlur', a, time);
    for (let x = 0; x < imageData.width; x++){
        for (let y = 0; y < imageData.height; y++){
            let pixeldata = getPixel(imageData, x, y);            
            for (let i = 0; i < 4; i++){
                pixeldata[i] = (1-a) * pixeldata[i] + a * (getPixel(imageData, x + deltax, y)[i] + getPixel(imageData, x - deltax, y)[i]
                 + getPixel(imageData, x, y + deltay)[i] + getPixel(imageData, x, y - deltay)[i])/4
            }
            setPixel(imageData, x, y, pixeldata);
        }
    }
    postMessage(imageData); 
}

setPixel = (imageData, x, y, pixeldata)=>{
    x = x * 4;                                      //each pixel has 4 bytes
    y = y * imageData.width * 4;                    //each line has width * 4 pixel
    for (let i = 0; i < 4; i++)
    imageData.data[x + y + i] = pixeldata[i];
}
getPixel = (imageData, x, y) => {
    if ((x < 0) || (y < 0) || (x > imageData.width) || (y > imageData.height))
        return [0, 0, 0, 0];                        //pixel is out of bounds, return 0 to RGBA
    x = x * 4;                                      //each pixel has 4 bytes
    y = y * imageData.width * 4;                    //each line has width * 4 pixel
    let ret = Array(4);                             //each pixel is an array of 4 bytes RGBA
    for (let i = 0; i < 4; i++)
        ret[i] = imageData.data[x + y + i] ;        //get the RGBA
    return ret;
}