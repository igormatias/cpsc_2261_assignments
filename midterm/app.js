"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Bot = /** @class */ (function () {
    function Bot(id, name) {
        this.id = id;
        this.name = name;
        this.x = 0;
        this.y = 0;
        this.frozen = false;
    }
    return Bot;
}());
var express = require("express");
var cors = require("cors");
var bodyParser = require("body-parser");
var app = express();
var id = 0;
var bots = [];
var map = [];
var corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200
};
app.use(cors(corsOptions));
app.use(bodyParser.json());
function checkBot(bot, x, y) {
    if (bot.x == x)
        if (bot.y == y)
            return true;
    return false;
}
function moveBot(bot, i) {
    if (i == 0) {
        bot.x -= 1;
        bot.y -= 1;
    }
    else if (i == 1) {
        bot.y -= 1;
    }
    if (i == 2) {
        bot.x += 1;
        bot.y -= 1;
    }
    //didnt finished!!!
}
//Add end points
app.get("/test", function (req, res) {
    res.send('{"test": 1 }');
});
app.get("/clear", function (req, res) {
    var count = id;
    id = 0;
    while (bots.length > 0)
        bots.pop();
    res.send({ "botskilled": count });
});
app.get("/create/:name", function (req, res) {
    res.header("Content-Type", "application/json");
    var bot = new Bot(id++, req.params.name);
    bots.push(bot);
    res.json({ id: bot.id, name: bot.name });
});
app.get("/list", function (req, res) {
    res.header("Content-Type", "application/json");
    var botList = "[";
    res.json(bots);
});
app.post("/map/:id", function (req, res) {
    res.header("Content-Type", "application/json");
    var paramId = req.params.id;
    if (paramId >= id) {
        res.send({ error: 'No bots with this id' });
    }
    else {
        var x = void 0;
        var y = void 0;
        x = bots[paramId].x - 1;
        y = bots[paramId].y - 1;
        if (checkBot(bots[paramId], x, y)) {
            map.push(1);
        }
        else {
            map.push(0);
        }
        x = bots[paramId].x;
        if (checkBot(bots[paramId], x, y)) {
            map.push(1);
        }
        else {
            map.push(0);
        }
        x = bots[paramId].x + 1;
        if (checkBot(bots[paramId], x, y)) {
            map.push(1);
        }
        else {
            map.push(0);
        }
        x = bots[paramId].x - 1;
        y = bots[paramId].y;
        if (checkBot(bots[paramId], x, y)) {
            map.push(1);
        }
        else {
            map.push(0);
        }
        x = bots[paramId].x + 1;
        if (checkBot(bots[paramId], x, y)) {
            map.push(1);
        }
        else {
            map.push(0);
        }
        x = bots[paramId].x - 1;
        y = bots[paramId].y + 1;
        if (checkBot(bots[paramId], x, y)) {
            map.push(1);
        }
        else {
            map.push(0);
        }
        x = bots[paramId].x;
        if (checkBot(bots[paramId], x, y)) {
            map.push(1);
        }
        else {
            map.push(0);
        }
        x = bots[paramId].x + 1;
        if (checkBot(bots[paramId], x, y)) {
            map.push(1);
        }
        else {
            map.push(0);
        }
        res.send({ endpoint: '/map', echo: map });
    }
});
app.post("/move/:id", function (req, res) {
    var paramId = req.params.id;
    if (paramId >= id) {
        res.send({ error: 'No bots with this id' });
    }
    else {
        if (!bots[paramId].frozen) {
            for (var i = 0; i < map.length; i++) {
                if (map[i] == 0) {
                    moveBot(bots[paramId], i);
                }
            }
        }
        //Didnt finishedd!!
    }
});
app.post("/fire/:id", function (req, res) {
    var paramId = req.params.id;
    if (paramId >= id) {
        res.send({ error: 'No bots with this id' });
    }
    else {
    }
});
app.post("/freeze/:id", function (req, res) {
    var paramId = req.params.id;
    if (paramId >= id) {
        res.send({ error: 'No bots with this id' });
    }
    else {
    }
});
//Start the server
app.listen(3436, function () {
    console.log("server started");
});
