"use strict";
var express = require("express");
var item_model_1 = require("../../util/item.model");
var bodyParser = require("body-parser");
var util_class_1 = require("../../util/util.class");
var router = express.Router();
router.use(bodyParser.json()); //Parse json http bodies
var ingredientsList;
var util = new util_class_1.Util();
ingredientsList = [
    new item_model_1.Ingredient('carrot', 2),
    new item_model_1.Ingredient('meat', 1),
    new item_model_1.Ingredient('potato', 1),
    new item_model_1.Ingredient('rice', 1),
    new item_model_1.Ingredient('garlic', 1),
    new item_model_1.Ingredient('onion', 1),
    new item_model_1.Ingredient('milk', 1),
];
router.get('/getIngredients', function (req, res) {
    res.header("Content-Type", "application/json");
    res.json(ingredientsList);
});
router.get('/test', function (req, res) {
    res.send('{"test": 1 }');
});
router.post('/addIngredient', function (req, res) {
    res.header("Content-Type", "application/json");
    var ingredient = req.body;
    var result = util.checkIfExist(ingredient, ingredientsList);
    util.addIngredientToList(ingredient, ingredientsList, result);
    res.send(ingredientsList);
});
router.post('/removeIngredient', function (req, res) {
    res.header("Content-Type", "application/json");
    var ingredient = req.body;
    var result = util.checkIfExist(ingredient, ingredientsList);
    util.removeIngredient(ingredient, ingredientsList, result);
    res.send(ingredientsList);
});
router.post('/editIngredient', function (req, res) {
    res.header("Content-Type", "application/json");
    ingredientsList[req.body['index']] = req.body['ingredient'];
    res.send(ingredientsList);
});
module.exports = router;
