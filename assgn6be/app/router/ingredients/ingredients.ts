import express = require('express');
import { Ingredient } from '../../util/item.model';
import bodyParser = require('body-parser');
import { Util } from '../../util/util.class';
import { Result } from '../../util/util.class';


let router = express.Router();
router.use(bodyParser.json()); //Parse json http bodies
let ingredientsList : Ingredient[];
let util = new Util();


ingredientsList = [
    new Ingredient('carrot', 2), 
    new Ingredient('meat', 1), 
    new Ingredient('potato', 1), 
    new Ingredient('rice', 1), 
    new Ingredient('garlic', 1), 
    new Ingredient('onion', 1), 
    new Ingredient('milk', 1),
];


router.get('/getIngredients', function(req, res){
    res.header("Content-Type","application/json");
    res.json(ingredientsList);
});

router.get('/test', function(req, res){
    res.send('{"test": 1 }');
});

router.post('/addIngredient', function(req, res){
    res.header("Content-Type","application/json");
    let ingredient = req.body;
    let result: Result = util.checkIfExist(ingredient, ingredientsList);
    util.addIngredientToList(ingredient, ingredientsList, result);
    res.send(ingredientsList);
});

router.post('/removeIngredient', function(req, res){
    res.header("Content-Type","application/json");
    let ingredient = req.body;
    let result: Result = util.checkIfExist(ingredient, ingredientsList);
    util.removeIngredient(ingredient, ingredientsList, result);
    res.send(ingredientsList);
});

router.post('/editIngredient', function(req, res){
    res.header("Content-Type","application/json");
    ingredientsList[req.body['index']] = req.body['ingredient'];
    res.send(ingredientsList);
});

export = router;