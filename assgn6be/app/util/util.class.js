"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var item_model_1 = require("./item.model");
var Result = /** @class */ (function () {
    function Result() {
        this.found = false;
        this.index = 0;
    }
    return Result;
}());
exports.Result = Result;
var Util = /** @class */ (function () {
    function Util() {
    }
    Util.prototype.checkIfExist = function (ingredient, ingredients) {
        var found = false;
        var index = -1;
        for (var i = 0; i < ingredients.length && !found; i++) {
            if (ingredients[i].name == ingredient.name) {
                index = i;
                found = true;
            }
        }
        return { found: found, index: index };
    };
    Util.prototype.addIngredientToList = function (ingredient, list, result) {
        if (result.found)
            list[result.index].quantity += ingredient.quantity;
        else
            list.push(ingredient);
    };
    Util.prototype.removeIngredient = function (ingredient, list, result) {
        if (result.found)
            list[result.index].quantity -= ingredient.quantity;
        if (list[result.index].quantity <= 0)
            list.splice(result.index, 1);
    };
    Util.prototype.copyIngredients = function (ingredients) {
        var newIngredients = [];
        ingredients.map(function (ingredient) {
            newIngredients.push(new item_model_1.Ingredient(ingredient.name, ingredient.quantity));
        });
        return newIngredients;
    };
    Util.prototype.copyInstructions = function (instructions) {
        var newInstructions = [];
        instructions.map(function (instruction) {
            newInstructions.push(instruction);
        });
        return newInstructions;
    };
    return Util;
}());
exports.Util = Util;
