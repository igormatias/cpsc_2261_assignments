//Testing node so we can use require!
//Browser is suppose to support require, but apparently no.
let request = require("request");

describe("test response suite", () => {
    it("send a test message and expect to get a test object", (done) => { //need done fo asynchronous tests. It's a function
        request.get("http://localhost:3000/ingredients/test", (error, response, body) => {
            expect(error).toBe(null); //No errors
            let data = JSON.parse(body);
            expect(data.test).toBe(1);
            done(); //need to call done at the end of a test, if there are callbacks.
        });
    })
});

describe("getIngredients test suite", () => {
    it("request a list of ingredients and expect to receive a list of ingredients", (done) => { //need done fo asynchronous tests. It's a function
        request.get("http://localhost:3000/ingredients/getIngredients", (error, response, body) => {
            expect(error).toBe(null); //No errors
            let data = JSON.parse(body);
            expect(data).toContain({ name: 'carrot', quantity: 2 });
            expect(data).toContain({ name: 'potato', quantity: 1 });
            expect(data).toContain({ name: 'rice', quantity: 1 });
            expect(data).toContain({ name: 'garlic', quantity: 1 });
            expect(data).toContain({ name: 'onion', quantity: 1 });
            expect(data).toContain({ name: 'milk', quantity: 1 });
            done(); //need to call done at the end of a test, if there are callbacks.
        });
    })
});

describe("addIngredient test suite", () => {
    it("add an ingredient to a list of ingredients and expect to receive a list of ingredients", (done) => {
        let newIngredient = { name: 'test', quantity: 1 };
        let options = { 
            uri: "http://localhost:3000/ingredients/addIngredient",
            method: "POST",
            json: newIngredient
        };
        request.post(options, (error, response, body) => {
            expect(error).toBe(null); //No errors
            expect(body).toContain({ name: 'test', quantity: 1 });
            done(); //need to call done at the end of a test, if there are callbacks.
        });
    })
});

describe("addIngredient test suite", () => {
    it("add an ingredient to a list of ingredients and expect to receive a list of ingredients within ingredient test have quantity 2", (done) => {
        let newIngredient = { name: 'carrot', quantity: 1 };
        let options = { 
            uri: "http://localhost:3000/ingredients/addIngredient",
            method: "POST",
            json: newIngredient
        };
        request.post(options, (error, response, body) => {
            expect(error).toBe(null); //No errors
            expect(body).toContain({ name: 'carrot', quantity: 3 });
            done(); //need to call done at the end of a test, if there are callbacks.
        });
    })
});

describe("removeIngredient test suite", () => {
    it("remove an ingredient to a list of ingredients and expect to receive a list of ingredients without test ingredient", (done) => {
        let newIngredient = { name: 'test', quantity: 1 };
        let options = { 
            uri: "http://localhost:3000/ingredients/removeIngredient",
            method: "POST",
            json: newIngredient
        };
        request.post(options, (error, response, body) => {
            expect(error).toBe(null); //No errors
            expect(body).not.toContain({newIngredient});
            done(); //need to call done at the end of a test, if there are callbacks.
        });
    })
});

describe("editIngredient test suite", () => {
    it("edit an ingredient to a list of ingredients and expect to receive a list of ingredients", (done) => {
        let reqEdit = { index: 1, ingredient: {
            name: 'meatToTest',
            quantity: 2
        }};
        let options = { 
            uri: "http://localhost:3000/ingredients/editIngredient",
            method: "POST",
            json: reqEdit
        };
        request.post(options, (error, response, body) => {
            expect(error).toBe(null); //No errors
            console.log(body);
            expect(body).toContain({ name: 'meatToTest', quantity: 2 });
            done(); //need to call done at the end of a test, if there are callbacks.
        });
    })
});