"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var app = require("./config/config");
var cors = require("cors");
var ingredient = require("./app/router/ingredients/ingredients");
app.use(cors());
app.use(function (req, res, next) {
    console.log('The request was received at ' + Date.now());
    next();
});
app.use('/ingredients', ingredient);
app.get('*', function (req, res) {
    res.send("not found");
});
app.listen(3000);
