import app = require('./config/config');
import cors = require('cors');
import ingredient = require('./app/router/ingredients/ingredients');



app.use(cors());


app.use(function(req, res, next){
    console.log('The request was received at ' + Date.now());
    next();
});

app.use('/ingredients', ingredient);

app.get('*',function(req, res){
    res.send("not found");
});

app.listen(3000);