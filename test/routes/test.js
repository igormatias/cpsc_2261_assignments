module.exports = (app) =>{
    app.get('/test', (req, res)=>{
        app.config.dbConnection();
        let testModel = app.models.testModel;
        testModel.getData();
        res.send('test ok');
    });
    app.post('/test/post', (req, res)=>{
        let answer = req.body;
        res.send([answer, {test: 1}]);
    });

};