let express = require('express');
let consign = require('consign');
let bodyParser = require('body-parser');
let cors = require('cors');

let app = express();

app.use(bodyParser());
app.use(cors());

consign()
    .include('routes')
    .then('config/dbConnection.js')
    .then('models')
    .into(app);
module.exports = app;