let mongo = require('mongodb');
dbConnection = function(){
    console.log('mongoDB connection');
    let db = new mongo.Db(
        'test',      //database name
        new mongo.Server(
            'localhost',        //server address
            27017,              //server port
            {}
        ),
        {}
    );
    return db;
}
module.exports = ()=>{
    return dbConnection;
};