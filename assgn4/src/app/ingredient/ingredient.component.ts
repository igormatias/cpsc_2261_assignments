import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Ingredient } from '../shared/item.model';
import { IngredientManagementService } from '../fridgeManager.service'

@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.css']
})
export class IngredientComponent implements OnInit {
  ingredients: Ingredient[] = [];
  ingName: string = '';
  ingQty: number;
  button: {value: string, class: string, index: number} = {value: " Add", class: "fa fa-floppy-o", index: 0};
  

  constructor(private ingredientManager: IngredientManagementService) {
    this.ingredients = ingredientManager.getIngredientsList();
  }

  addIng(index: number, quantity: number){
    let ingredient: Ingredient = new Ingredient(this.ingredients[index].name, 1);
    this.ingredientManager.addIngredient(ingredient);
  }
  
  removeIng(index: number){
    let ingredient: Ingredient = new Ingredient(this.ingredients[index].name, 1);
    this.ingredientManager.removeIngredient(ingredient);
  }

  removeAll(index: number){
    this.ingredientManager.removeIngredient(this.ingredients[index]);
  }

  addNewIng(){
    let ingredient = new Ingredient(this.ingName, Number(this.ingQty));
    if (this.button.value == 'Add'){
      this.ingredientManager.addIngredient(ingredient);
    }
    else{
      this.button.value = ' Add';
      this.button.class = 'fa fa-floppy-o';
      this.ingredientManager.editIngredient(this.button.index, ingredient);
    }
    this.ingName = '';
    this.ingQty = 0;
  }

  editIng(index: number){
    this.ingName = this.ingredients[index].name;
    this.ingQty = this.ingredients[index].quantity;
    this.button.value = " Edit"
    this.button.class = "fa fa-pencil";
    this.button.index = index;
  }

  addToRecipe(index: number){
    this.ingredientManager.sendToRecipe(this.ingredients[index]);

  }

  ngOnInit() {
  }

}
