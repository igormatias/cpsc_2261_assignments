import {Ingredient, Recipe } from './shared/item.model'
import { RecipeComponent } from './recipe/recipe.component'
import { FridgeComponent } from './fridge/fridge.component'


class Result{
    found: boolean;
    index: number;
}

class Util{
    checkIfExist(ingredient: Ingredient, ingredients: Ingredient[]): Result{
        let found: boolean = false;
        let index: number = -1;
        for (let i: number = 0; i < ingredients.length && !found; i++){
            if (ingredients[i].name == ingredient.name){
                index = i;
                found = true;
            }
        }
        return {found, index};
    }

    addIngredientToList(ingredient: Ingredient, list: Ingredient[], result: Result){
        if (result.found)
            list[result.index].quantity += ingredient.quantity;
        else
            list.push(ingredient);
    }

    removeIngredient(ingredient: Ingredient, list: Ingredient[], result: Result){
        if (result.found)
            list[result.index].quantity -= ingredient.quantity;
        if (list[result.index].quantity <= 0)
            list.splice(result.index, 1);
    }
    copyIngredients(ingredients: Ingredient[]): Ingredient[]{
        let newIngredients: Ingredient[] = [];
        ingredients.map( (ingredient: Ingredient) =>{
            newIngredients.push(new Ingredient(ingredient.name, ingredient.quantity));
        })
        return newIngredients;
    }

    copyInstructions(instructions: string[]): string[]{
        let newInstructions: string[] = [];
        instructions.map( (instruction: string) =>{
            newInstructions.push(instruction);
        })
        return newInstructions;
    }
}
export class FridgeManagementService{
    private util = new Util();
    ingredients: Ingredient[] = [];
    

    addIngredient(ingredient: Ingredient, ingredients: Ingredient[]){
        let result: Result = this.util.checkIfExist(ingredient, ingredients)
        this.util.addIngredientToList(ingredient, ingredients, result);
    }

    removeIngredient(ingredient: Ingredient, ingredients: Ingredient[]) {
        let result: Result = this.util.checkIfExist(ingredient, ingredients)
        this.util.removeIngredient(ingredient, ingredients, result);
    }
    

    checkRecipe(recipeIngredients: Ingredient[]): {shoppingList: Ingredient[], fridgeIngredients: Ingredient[]}{
        //return two arrays
        //the array in index 0 is the shopping list
        //the array in index 1 is the ingredients that are already in the fridge
        let shoppingList: Ingredient[] = [];
        let fridgeIngredients: Ingredient[] = this.util.copyIngredients(this.ingredients);
        recipeIngredients.map( (ingFromRecipe: Ingredient) =>{
            let result: {found: boolean, index: number} = this.util.checkIfExist(ingFromRecipe, this.ingredients);
            if (!result.found)
                shoppingList.push(ingFromRecipe);
            else{
                let quantity = ingFromRecipe.quantity - this.ingredients[result.index].quantity;
                if (quantity > 0)
                    shoppingList.push(new Ingredient(ingFromRecipe.name, quantity));
            }
        });
        return {shoppingList, fridgeIngredients};
    }
    

}
export class RecipeManagementService{
    recipesList: Recipe[] = [];
    private fridgeComponent: FridgeComponent;
    private util = new Util();
    
    getFridge(fridge: FridgeComponent){
        this.fridgeComponent = fridge;
    }

    addIngredient(ingredient: Ingredient, recipe: Recipe){
        let result: Result = this.util.checkIfExist(ingredient, recipe.ingredients)
        this.util.addIngredientToList(ingredient, recipe.ingredients, result);
    }

    removeIngredient(ingredient: Ingredient, recipe: Recipe) {
        let result: Result = this.util.checkIfExist(ingredient, recipe.ingredients)
        this.util.removeIngredient(ingredient, recipe.ingredients, result);
    }

    addInstruction(instruction: string, recipe: Recipe){
        recipe.instructions.push(instruction);
    }

    removeInstruction(index: number, recipe: Recipe) {
        recipe.instructions.splice(index, 1);
    }

    editInstruction(index: number, instruction: string, recipe: Recipe){
        recipe.instructions[index] = instruction;
    }

    getRecipeList(): Recipe[]{
        return this.recipesList;
    }

    getNewRecipe(){
        return new Recipe('',[],[],'');
    }

    addnewRecipe(recipe: Recipe){
        this.recipesList.push(recipe);
    }

    getRecipe(index: number): Recipe{
        //need to return a new object with new arrays
        let recipe: Recipe = new Recipe(
            this.recipesList[index].name, 
            this.util.copyIngredients(this.recipesList[index].ingredients),
            this.util.copyInstructions(this.recipesList[index].instructions),
            this.recipesList[index].estimatedTime);
        return recipe;
    }

    update(index: number, recipe: Recipe){
        this.recipesList[index] = recipe;
    }

    remove(index: number){
        this.recipesList.splice(index, 1);
    }

    addIngredientToFridge(ingredient: Ingredient, ingredients: Ingredient[]){
        let result: Result = this.util.checkIfExist(ingredient, ingredients)
        this.util.addIngredientToList(ingredient, ingredients, result);
    }
    

    sendToFridge(recipe: Recipe){
        let newIngredients: Ingredient[] = this.util.copyIngredients(recipe.ingredients);
        newIngredients.map( (newIngredient) =>{
            this.addIngredientToFridge(newIngredient, this.fridgeComponent.recipeIngredients)            
        })
        //this.fridgeComponent.recipeIngredients = this.util.copyIngredients(recipe.ingredients);
        this.fridgeComponent.updateLists();
    }
}    

export class IngredientManagementService{
    private ingredientsList: Ingredient[];
    private util = new Util();
    private recipeComponent: RecipeComponent;
    private recipeManagement = new RecipeManagementService();

    constructor(){
    this.ingredientsList = [
        new Ingredient('carrot', 1), 
        new Ingredient('meat', 1), 
        new Ingredient('potato', 1), 
        new Ingredient('rice', 1), 
        new Ingredient('garlic', 1), 
        new Ingredient('onion', 1), 
        new Ingredient('milk', 1),
    ];
    }

    addIngredient(ingredient: Ingredient) {
        let result: Result = this.util.checkIfExist(ingredient, this.ingredientsList);
        this.util.addIngredientToList(ingredient, this.ingredientsList, result);
    }

    removeIngredient(ingredient: Ingredient){
        let result: Result = this.util.checkIfExist(ingredient, this.ingredientsList);
        this.util.removeIngredient(ingredient, this.ingredientsList, result);
    }
    
    getIngredientsList(): Ingredient[]{
        return this.ingredientsList;
    }
    
    editIngredient(index: number, ingredient: Ingredient){
        this.ingredientsList[index] = ingredient;
    }

    getRecipe(recipe: RecipeComponent){
        this.recipeComponent = recipe;
    }

    sendToRecipe(ingredient: Ingredient){
        let newIngredient: Ingredient = new Ingredient(ingredient.name, ingredient.quantity);
        this.recipeManagement.addIngredient(newIngredient, this.recipeComponent.recipe);

    }
}