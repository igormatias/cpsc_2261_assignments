import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { RecipeComponent } from './recipe/recipe.component';
import { IngredientComponent } from './ingredient/ingredient.component';
import { FridgeComponent } from './fridge/fridge.component';

import { IngredientManagementService, RecipeManagementService, FridgeManagementService } from './fridgeManager.service';

@NgModule({
  declarations: [
    AppComponent,
    RecipeComponent,
    IngredientComponent,
    FridgeComponent
  ],
  imports: [
    BrowserModule, 
    FormsModule
  ],
  providers: [IngredientManagementService, RecipeManagementService, FridgeManagementService],
  bootstrap: [AppComponent]
})
export class AppModule { }
