import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {FormsModule } from '@angular/forms'

import { RecipeComponent } from './recipe.component';
import Ingredient from '../ingredient/ingredient.class';

describe('RecipeComponent', () => {
  let component: RecipeComponent;
  let fixture: ComponentFixture<RecipeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipeComponent ],
      imports: [FormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add one ingredient to the recipe', () => {
    component.getFromMarket(new Ingredient('testIngredient', 1));
    expect(component.recipe.ingredients).toContain(new Ingredient('testIngredient', 1));
  });

  it('should increase the amount of ingredient in the index 0', () => {
    component.recipe.ingredients.push(new Ingredient('',1));
    component.addIng(0, 1);
    expect(component.recipe.ingredients[0].quantity).toBe(2);
  });

  it('should decrement the amount of ingredient in the index 0', () => {
    component.recipe.ingredients.push(new Ingredient('',2));
    component.removeIng(0);
    expect(component.recipe.ingredients[0].quantity).toBe(1);
  });

  it('should decrement the amount of ingredient in the index 0 and remove it as the amount become 0', () => {
    let ing = new Ingredient('testIng',1);
    component.recipe.ingredients.push(ing);
    component.removeIng(0);
    ing.quantity--;
    expect(component.recipe.ingredients).not.toContain(ing);
    expect(component.recipe.ingredients.length).toBe(0);
  });

  it('should remove the ingredient from the recipe ingredients list', () => {
    const ing = new Ingredient('testIng',1);
    const ing2 = new Ingredient('testIng 2',1);
    
    component.recipe.ingredients.push(ing);
    component.recipe.ingredients.push(ing2);
    component.removeAll(0);
    expect(component.recipe.ingredients).not.toContain(ing);
    expect(component.recipe.ingredients.length).toBe(1);
    expect(component.recipe.ingredients).toContain(ing2);
  });

  it('should remove the instruction from the recipe instruction list', () => {
    const inst: string = 'inst';
    const inst2: string = 'instr 2';

    component.recipe.instructions.push(inst);
    component.recipe.instructions.push(inst2);
    component.removeInstr(0);
    expect(component.recipe.instructions).not.toContain(inst);
    expect(component.recipe.instructions.length).toBe(1);
    expect(component.recipe.instructions).toContain(inst2);
  });
  
  it('should add new instruction from the recipe instruction list', () => {
    const inst: string = 'inst';
    const inst2: string = 'instr 2';

    component.recipe.instructions.push(inst);
    component.recipe.instructions.push(inst2);
    component.instructionInput = 'new Instruction'
    component.addNewInstruction();
    expect(component.recipe.instructions.length).toBe(3);
    expect(component.recipe.instructions).toContain('new Instruction');
  });

  it('should edit a instruction from the recipe instruction list', () => {
    const inst: string = 'inst';
    const inst2: string = 'instr 2';

    component.recipe.instructions.push(inst);
    component.recipe.instructions.push(inst2);
    component.instructionInput = 'new Instruction'
    component.button.value = 'Edit'
    component.button.index = 0;
    component.addNewInstruction();
    expect(component.recipe.instructions.length).toBe(2);
    expect(component.recipe.instructions).toContain('new Instruction');
    expect(component.recipe.instructions).not.toContain('instr');
  });

  it('should raise and event with the ingredient in the index number', () => {
    let index: number = null;
    const ing = new Ingredient('testIng',1);
    const ing2 = new Ingredient('testIng 2',1);
    
    component.recipe.ingredients.push(ing);
    component.recipe.ingredients.push(ing2);

    component.sendToFridge.subscribe( sf => index = sf);
    component.addToFridge(1);

    expect(index).toBe(ing2);
  });

  it('should raise and event with the ingredient list', () => {
    let ingredients: Ingredient[];
    const ing = new Ingredient('testIng',1);
    const ing2 = new Ingredient('testIng 2',1);
    
    component.recipe.ingredients.push(ing);
    component.recipe.ingredients.push(ing2);

    component.updateFridge.subscribe( ingList => ingredients = ingList);
    component.update(component.recipe.ingredients);

    expect(ingredients).not.toBeNull;
    expect(ingredients.length).toBe(2);
    expect(ingredients).toContain(ing);
    expect(ingredients).toContain(ing2);
  });
});
