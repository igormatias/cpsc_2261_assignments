import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import { Recipe, Ingredient } from '../shared/item.model'
import { RecipeManagementService, IngredientManagementService } from '../fridgeManager.service'


@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.css']
})


export class RecipeComponent implements OnInit {
  recipe: Recipe = new Recipe('',[],[],'');
  recipesList: Recipe[] = [];
  editEnabled: boolean = false;
  instructionInput: string;
  recipeSelectedIndex: number;
  button: {value: string, class: string, index: number} = {value: " Add", class: "fa fa-floppy-o", index: 0};
  buttonRecipe: {value: string, class: string, index: number} = {value: "btn btn-primary", class: "fa fa-check", index: 0};
  
  constructor(private RecipeManagement: RecipeManagementService, private ingredientManager: IngredientManagementService) {
    this.recipe = this.RecipeManagement.getNewRecipe();
    this.recipesList = this.RecipeManagement.getRecipeList();
  }

  addIng(index: number){
    let ingredient = new Ingredient(this.recipe.ingredients[index].name, 1)
    this.RecipeManagement.addIngredient(ingredient ,this.recipe);
    this.sendToFridge();
  }
  
  removeIng(index: number){
    let ingredient = new Ingredient(this.recipe.ingredients[index].name, 1)
    this.RecipeManagement.removeIngredient(ingredient ,this.recipe);
    this.sendToFridge();
    
  }

  removeAll(index: number){
    let ingredient = new Ingredient(this.recipe.ingredients[index].name, this.recipe.ingredients[index].quantity)
    this.RecipeManagement.removeIngredient(ingredient ,this.recipe);    
    this.sendToFridge();
  }

  editInstr(index: number){
    this.instructionInput = this.recipe.instructions[index];
    this.button.value = " Edit"
    this.button.class = "fa fa-pencil";
    this.button.index = index;
  }

  removeInstr(index: number){
    this.RecipeManagement.removeInstruction(index, this.recipe);
  }

  selectRecipe(index: number){
    this.recipeSelectedIndex = index;
    this.recipe = this.RecipeManagement.getRecipe(index);
  }

  addNewInstruction(){
    if (this.button.value == "Add")
      this.recipe.instructions.push(this.instructionInput);
    else{
      this.button.value = "Add";
      this.button.class = "fa fa-floppy-o";
      this.RecipeManagement.editInstruction(this.button.index, this.instructionInput, this.recipe);
    }
    this.instructionInput = "";  
  }

  addNewRecipe(){
    this.RecipeManagement.addnewRecipe(this.recipe);
    this.clearRecipe();
  }

  clearRecipe(){
    this.recipe = this.RecipeManagement.getNewRecipe();
  }

  editRecipe(){
    this.recipe = this.RecipeManagement.getRecipe(this.recipeSelectedIndex);
    this.editEnabled = true;
  }

  updateRecipe(){
    this.RecipeManagement.update(this.recipeSelectedIndex, this.recipe);
    this.editEnabled = false;
    this.clearRecipe();
  
  }

  sendIngredientToFridge(index: number){
    let recipe = new Recipe('',[this.recipe.ingredients[index]],[],'');
    this.RecipeManagement.sendToFridge(recipe);
  }

  removeRecipe(){
    this.RecipeManagement.remove(this.recipeSelectedIndex);
  }

  sendToFridge(){
    this.RecipeManagement.sendToFridge(this.recipe);
  }

  sendRecipeToFridge(){
    this.RecipeManagement.sendToFridge(this.recipesList[this.recipeSelectedIndex]);
  }
  
  ngOnInit() {
    this.ingredientManager.getRecipe(this);
  }
}
