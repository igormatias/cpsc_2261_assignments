import Ingredient from '../ingredient/ingredient.class';
import Recipe from '../recipe/recipe.class';

export default class Fridge{
    public contents: Ingredient[] = [];

    private removeIngredient(index: number) {
        this.contents.splice(index, 1);
    }

    private addIfExist(ingredient: Ingredient): boolean{
        let result : {found: boolean, index: number} = this.checkIfExist(ingredient);
        if (result.found)
            this.contents[result.index].quantity += ingredient.quantity;
        return result.found;
    }
    
    private removeIfExist(ingredient: Ingredient): {found: boolean, index: number, quantity: number}{
        let found: boolean = false;
        let index: number = -1;
        let quantity: number = 0;
        let result : {found: boolean, index: number} = this.checkIfExist(ingredient);
            if (result.found){
                this.contents[result.index].quantity -= ingredient.quantity;
                quantity = this.contents[result.index].quantity;
                index = result.index;
                found = true;
        }
        return {found, index, quantity};
    }

    private checkIfExist(ingredient: Ingredient): {found: boolean, index: number}{
        let found: boolean = false;
        let index: number = -1;
        for (let i: number = 0; i < this.contents.length && !found; i++){
            if (this.contents[i].name == ingredient.name){
                index = i;
                found = true;
            }
        }
        return {found, index};
    }

    add(ingredient: Ingredient): void{
        if (!this.addIfExist(ingredient))
        this.contents.push(ingredient);
    }
    remove(ingredient: Ingredient): boolean{
        let result : {found: boolean, index: number, quantity: number} = this.removeIfExist(ingredient);
        if (result.found ){
            if (result.quantity <= 0)
                this.removeIngredient(result.index);
        }
        return result.found;
    }
}