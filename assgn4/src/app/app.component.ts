import { Component, ViewChild, OnInit } from '@angular/core';
import Ingredient from './ingredient/ingredient.class';
import { RecipeComponent } from './recipe/recipe.component';
import { FridgeComponent } from './fridge/fridge.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Fridge Management';
  
  @ViewChild(RecipeComponent) recipeComponent: RecipeComponent;
  @ViewChild(FridgeComponent) fridgeComponent: FridgeComponent;

  ngOnInit(){
  }
  
}
